FROM openjdk:8-jdk-alpine
ARG JAVA_FILE
COPY ${JAVA_FILE} app.jar
EXPOSE 8080


ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar","app.jar","--imageServer=http://3.86.38.246:8082/images/"]

